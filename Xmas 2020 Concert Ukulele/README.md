Go to:  https://kms104.gitbook.io/3d-printing-ukuleles/ for a more detailed description of the ukuleles, printer settings, and files.

You can here examples of these ukuleles being played at: https://www.youtube.com/playlist?list=PLXbvWuus8wwvp80Zf3ysKaLMbjueqepfr

Generally I use:

*.4 mm nozzle
*0.30 mm "DRAFT" layer height
*Different amounts of infill depending on the piece
*Circular disks at the sharp corners of the large pieces to prevent warping on multi-hour prints
*Magnets on the circular disks after the print layer exceeds 3mm to help hold print down and reduce warping on long prints
*Super glue to glue pieces together
*Texture print sheet

Assembling Ukulele Notes:
Neck and Fretboard
*Use carbon fiber bars to strength the neck - see gitbook for details
*Spaces/voids in the ends of the neck were to fill with epoxy for extra strength - it works but I no longer think this is necessary
*Fretboard is glued to the neck and then the neck is slid into the body and secured with a M5 bolt
*Nut can be swapped out to tune the instruments action and feel

Body
*Print with extra perimeters to make sure the body does not have voids and is as still as possible
*Use super glue to assembly the upper and lower body parts
*Print the lower part of the body with the string mounts from the Saddle/Bridge/Break - but not the Saddle itself
*Print the Saddle seperately and bolt to the body - you can experiment to see how the height impacts the action, sound, and buzzing
*I printed one saddle that uses a vertically mounted piece of the carbon fiber stiffening bars from the neck - I cannot tell much difference

