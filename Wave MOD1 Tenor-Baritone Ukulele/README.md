Go to:  https://kms104.gitbook.io/3d-printing-ukuleles/ for a more detailed description of the ukuleles, printer settings, and files.

*.3MF files are the Prusa Slicer (2.3) project files that include the filament parameters, printer settings, and modifiers I use to successfully and somewhat reliably print the ukulele parts.

I am still tweaking the Nut design and the saddle design as I changed strings and try different configurations to adjust the action and reduce buzzing.

Generally I use:
- .4 mm nozzle
- 0.30 mm "DRAFT" layer height
- Different amounts of infill depending on the piece
- Circular disks at the sharp corners of the large pieces to prevent warping on multi-hour prints
- Texture print sheet

